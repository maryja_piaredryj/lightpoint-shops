angular.module('lightpointTestApp')
  .service('shopsService', function () {

    var shops = [
      {
        id: 1,
        name: 'Doe Shop',
        address: 'ABC street, 15',
        hours: '09 - 21',
        products: [
          {
            name: 'Cell Phone',
            description: '$50, brand new product'
          },
          {
            name: 'Magic Ball',
            description: '$10, knows everything about you'
          }
        ]
      },
      {
        id: 2,
        name: 'Lipsum',
        address: 'Privet drive, 4',
        hours: '11 - 23',
        products: [
          {
            name: 'Queen Jewels',
            description: '$2550, generic jewels'
          },
          {
            name: 'Robot',
            description: '$2100, robot whos humor level is over 100%'
          },
          {
            name: 'Lamp',
            description: '$300, magic'
          }
        ]
      },
      {
        id: 3,
        name: 'Epay',
        address: 'Baker street, 221',
        hours: '08 - 20',
        products: [
          {
            name: 'Lamborghini Gallardo',
            description: '$500 000, nothing special'
          }
        ]
      }
    ];

    this.getShops = function () {
      return shops;
    };



  });