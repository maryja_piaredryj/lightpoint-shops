'use strict';

/**
 * @ngdoc overview
 * @name lightpointTestApp
 * @description
 * # lightpointTestApp
 *
 * Main module of the application.
 */
angular
  .module('lightpointTestApp', [
    'ngCookies',
    'ngRoute',
    'ngAnimate',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.sortable'
  ])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/products/:shopID', {
        templateUrl: 'views/products.html',
        controller: 'ProductsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
  });