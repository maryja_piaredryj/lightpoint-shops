'use strict'

angular.module('lightpointTestApp')
  .controller('AboutCtrl', ['$scope', 'shopsService', function ($scope, shopsService) {
    $scope.shops = [];

    $scope.shops = shopsService.getShops();
  }]);