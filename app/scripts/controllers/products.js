'use strict'

angular.module('lightpointTestApp')
  .controller('ProductsCtrl', ['$scope', '$routeParams', 'shopsService', function ($scope, $routeParams, shopsService) {

    $scope.shops = [];
    $scope.shop = {};

    $scope.shops = shopsService.getShops();

    var shopID = $routeParams.shopID;

    $scope.getShop = function (id) {
      for (var i = 0; i < $scope.shops.length; i++) {
        if ($scope.shops[i].id === parseInt(id)) {
          return $scope.shops[i];
        }
      }
      return null;
    };

    $scope.shop = $scope.getShop(shopID);

    $scope.removeProduct = function (index) {
      $scope.shop.products.splice(index, 1);
    };

    $scope.addProduct = function () {
      var name = $scope.newProduct.name;
      var description = $scope.newProduct.description;
      var newProduct = {
        name: name,
        description: description,
      }
      $scope.shop.products.push(newProduct);
      $scope.newProduct.name = '';
      $scope.newProduct.description = '';
    };

  }]);