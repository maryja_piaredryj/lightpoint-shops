'use strict'

angular.module('lightpointTestApp')
  .controller('MainCtrl', ['$scope', 'shopsService', function ($scope, shopsService) {

    $scope.shops = shopsService.getShops();
    $scope.addShop = function () {
      var name = $scope.newShop.name;
      var address = $scope.newShop.address;
      var hours = $scope.newShop.hours;
      var newShop = {
        id: $scope.shops.length + 1,
        name: name,
        address: address,
        hours: hours,
        products: []
      }
      $scope.shops.push(newShop);
      $scope.newShop.name = '';
      $scope.newShop.address = '';
      $scope.newShop.hours = '';
    };

    $scope.removeShop = function (index) {
      $scope.shops.splice(index, 1);
    };

}]);